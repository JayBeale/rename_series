# rename_series

```
usage: rename_series.py [-h] [-c CSV] [-d DIR] [-s SHOW] [--dry]

            Utility for renaming seasons of TV series in a format Plex expects.
            All files should be in a single directory, sorting by name in the episode order.

            A csv file in the same directory must be in the format of
            season number, episode number, episode title


optional arguments:
  -h, --help            show this help message and exit
  -c CSV, --csv CSV     Define csv file location.
  -d DIR, --dir DIR     Directory containing the files to be renamed and configured csv.
  -s SHOW, --show SHOW  Specify the show name. Required.
  --dry                 Just prints what the rename would look like, does not change file names.
```